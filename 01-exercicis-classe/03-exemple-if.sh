#! /bin/bash
# isx41016667 M01-ISO
# Febrer 2021
# Exemple if: indica si es major d'edat
#   $ prog edat
# -------------------------------------
# 1) validem arguments
if [ $# -ne 1 ]
then
  echo "Error: nº arguments incorrecte"
  echo "Usage: $0 edat"
  exit 1
fi

# xixa
edat=$1
if [ $edat -ge 18 ]
then
  echo "Edat $edat major d'edat"
fi
exit 0
