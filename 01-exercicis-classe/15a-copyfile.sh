#! /bin/bash
# isx41016667 M01-ISO
# Febrer 2021
# Llistar un directori
#       prog.sh file dirdesti
# Validar arguments
# Validar si es un fitxer
# Validar si es un directori
# Copiar el fitxer al directori desti
# -------------------------------------------
status=0
ERR_NARGS=1
ERR_NFILE=2
ERR_NDIR=3

# 1) validar num d'arguments
if [ $# -ne 2 ]; then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 file dir"
  exit $ERR_NARGS
fi

# 2) validar si es un regular file
if [ ! -e $1 ]; then
  echo "Error: fitxer $1 no existeix"
  echo "Usage: $0 file dir"
  exit $ERR_NFILE
fi
myfile=$1

# 3) validar si es un directori
if [ ! -d $2 ]; then
  echo "Error: $2 no es un directori"
  echo "Usage: $0 file dir"
  exit $ERR_NDIR
fi
mydir=$2

# xixa
cp $myfile $mydir
exit $status
