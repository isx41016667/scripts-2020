#! /bin/bash
# isx41016667 M01-ISO
# Febrer 2021
# Validar nota:
#   prog.sh nota
# Mostrar si esta aprovat/suspès
# Validar rep un arg
# i que la nota es un valor vàlid 0-10
# ------------------------------------
ERR_NARGS=1
ERR_VALOR=2

# 1) validar arguments
if [ $# -ne 1 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 nota"
  exit $ERR_NARGS
fi

# 2) validar nota pren un valor valid [0-10]
nota=$1
if ! [ $nota -ge 0 -a $nota -lt 10 ]
then
  echo "Error: nota no valida"
  echo "Usage: $0 nota [0-10]"
  exit $ERR_VALOR
fi

# 3) xixa
if [ $nota -lt 5 ]
then
  echo "Suspès"
else
  echo "Aprovat"
  exit 0
fi
