#! /bin/bash
# isx41016667 M01-ISO
# Febrer 2021
# Exemples de bucle while
# -------------------------------------------
# while condicio
# do
# 	accions
# done
# 9) numerar stdin linia a linia i mostrar-la en majuscules
num=1
while read -r line
do
  echo "$num: $line" | tr '[:lower:]' '[:upper:]'
  ((num++))
done
exit 0

# 8) mostrar stdin fins el token FI
read -r line
while [ "$line" != "FI" ]
do
  echo "$line"
  read -r line
done
exit 0

# 7) numerar la entrada estandar linia a linia
num=1
while read -r line
do
  echo "$num: $line"
  ((num++))
done
exit 0

# 6) mostra la entrada estandar linia a linia
while read -r line
do
  echo $line
done
exit 0

# 5)
num=1
while [ $# -ge 0 ]
do
  echo "$num: $1, $#,$* "  
  ((num++))
  shift
done
exit 0


# 4) esta biuda o no
while [ -n "$1" ]
do
  echo "$#: $* "
  shift
done
exit 0

# 3) mostra arguments
while [ $# -gt 0 ]
do
  echo "$#: $* "
  shift
done
exit 0

# 2) mostrar de n a 0 count down
MIN=0
num=$1
while [ $num -gt $MIN ]
do
  echo $num
  ((num--))
done
exit 0

# 1) mostrar numeros del 1 al 10
MAX=10
num=1
while [ $num -le $MAX ]
do
  echo $num
  ((num++))
done
exit 0
