#! /bin/bash
# isx41016667 M01-ISO
# Febrer 2021
# Mostrar els dies d'un mes:
#        prog mes
# Validar arguments
# Si es mes del any [1-12]
# Mostrar els dies del mes
# -------------------------------------------
ERR_NARGS=1
ERR_NMES=2
OK=0

# 1) Validar argument
if [ $# -ne 1 ]; then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 mes"
  exit $ERR_NARGS
fi

mes=$1
# 2) Validar si es mes del any
if ! [ $mes -ge 1 -a $mes -le 12 ]; then
  echo "Error: $1 no es un mes de l'any"
  echo "Usage: $0 mes (valor de [1,12])"
  exit $ERR_NMES
fi

# xixa
case $mes in
  "2")
    dies=38;;
  "4"|"6"|"9"|"11")
    dies=30;;
  *)
    dies=31
esac
echo "El mes $mes te $dies dies"

exit $OK
