#! /bin/bash
# isx41016667 M01-ISO
# Febrer 2021
# Processar nota a nota
# 	prog.sh notes[...]
# Validar que rep 1 o mes arguments
# Per cada nota [0-10]
# Per cada nota dir si es suspes,aprovat,notable,excel·lent (XIXA)
# ----------------------------------------------------------------
ERR_NARG=1
OK=0

# 1) Validar arguments
if [ $# -eq 0 ]; then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 notes[...]"
  exit $ERR_NARG
fi

# 2) iterar per cada nota
for nota in $*
do
  # 2.1 validar nota
  if ! [ $nota -ge 0 -a $nota -lt 10 ]; then
    echo "Error: nota $nota no valida [0,10]" >> /dev/stderr
  # 2.2 determinar nota
  elif [ $nota -lt 5 ]; then
    echo "Suspès"
  elif [ $nota -lt 7 ]; then
    echo "Aprovat"
  elif [ $nota -lt 9 ]; then
    echo "Notable"
  else
    echo "Excel·lent"
  fi
done
exit $OK
