#! /bin/bash
# isx41016667 M01-ISO
# Febrer 2021
# Si es un file,directori o link:
#	 prog file
# Validar arguments
# Si existeix el file
# Dir si es un regular file,dir o link
# -------------------------------------------
ERR_NARGS=1
ERR_NEXIST=2

# 1) validar arguments
if [ $# -ne 1 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 file/dir/link"
  exit $ERR_NARGS
fi

# 2) validar si existeix el file
if [ ! -e $1 ]
then
  echo "Error: $1 no existeix"
  echo "Usage: $0 file/dir/link"
  exit $ERR_EXIST
fi

# xixa
myfile=$1
if [ -L $myfile ]
then
  echo "$myfile es un link"
elif [ -d $myfile ]
then
  echo "$myfile es un directori"
else
  echo "$myfile es un regular file"
fi

exit 0

'''
# xixa
if [ ! -e $myfile ]; then
  echo "Error: $myfile no existeix"
  exit $ERR_NEXIST
elif [ -h $myfile ]; then
  echo "$myfile es un link"
elif [ -f $myfile ]; then
  echo "$myfile es regular"
elif [ -d $myfile ]; then
  echo "$myfile es un directori"
else
  echo "$myfile es una altre tipus de fitxer"
fi
exit 0
'''
