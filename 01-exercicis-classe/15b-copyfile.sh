#! /bin/bash
# isx41016667 M01-ISO
# Febrer 2021
# Copiar file a dirdesti
#       prog.sh file[...] dirdesti
# Mostra num arguments
# Mostra llista arguments
# Mostra l'ultim argument
# Mostra llista de fitxers
# -------------------------------------------
status=0
ERR_NARGS=1
ERR_NFILE=2
ERR_NDIR=3

# 1) validar num d'arguments
if [ $# -lt 2 ]; then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 file dir"
  exit $ERR_NARGS
fi

# xixa
# Mostra num arguments
echo "Num arguments: $#"

# Mostra llista d'arguments
echo "Llista arguments: $*"

# Mostrar l'ultim argument
echo "Ultim argument: $(echo $* | sed 's/^.* //')"
echo $* | cut -d' ' -f$#

# Mostra llista de fitxers
echo "Llista fitxers: $( echo $* | sed 's/ [^ ]*$//' )"
echo $* | cut -d' ' -f1-$(($#-1))
