# M01 ISO Operatius
## @edt Curs 2020-21
## Cristian Condolo Jimenes

### Exercicis Scripts

**01-exemple.sh** primer exemple:
- programa principal: Hello World
- variables
- ordres internes
- expansions (command expansion, arithmetic expansion)

**02-exemple-args.sh** exemples amb arguments

**03-exemple-if.sh** exemples amb if

**04-validar-argument.sh** exercici sobre arguments

**05-validar-nota** exercici amb if

**06-nota.sh** exercici amb if/elif/else

**07-esdirectori.sh** exercici sobre directoris

**07bis-esdirectori.sh**

**08-tipusfile.sh** exercici sobre files/dirs/links

**09-exemple-case.sh** exemples amb case

**10-case-dies-mes.sh** exercici amb case

**11-exemples-for.sh** exemples amb bucle for

**12-for-notas.sh** exercici amb bucle for

**12b-for-mesos.sh**

**13-exemples-while.sh** exemples amb bucle while
