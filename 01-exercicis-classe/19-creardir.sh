#! /bin/bash
# isx41016667 M01-ISO
# Març 2021
# Crea nou directoris amb arguments
#       prog.sh noudir[...]
# Validar num argument
# Validar si no existeix directori
# Crear nou directoris
# -------------------------------------------
status=0
ERR_NARGS=1
ERR_NMKDIR=2

# 1) validar num arguments
if [ $# -eq 0 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 noudir[...]"
  status=$ERR_NARGS
  exit $status
fi

directoris=$*
for noudir in $directoris
do
  # 2) validar si s'ha creat directori
  mkdir $noudir &> /dev/null
  if [ $? -eq 0 ]
  then
    echo "Directori $noudir Creat!"
  else
    echo "Error en crear $noudir" >> /dev/stderr
    status=$ERR_NMKDIR
  fi
done
exit $status
