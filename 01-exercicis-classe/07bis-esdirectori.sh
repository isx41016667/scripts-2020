#! /bin/bash
# isx41016667 M01-ISO
# Febrer 2021
# Llista el directori si existeix
#   prog.sh dir
# Validar hi ha un argument
# Si l'argument rebut es un directori si no es directori error.
# Llistar-ho.
# ------------------------------------
ERR_NARGS=1
ERR_NODIR=2

# 1) validar arguments
if [ $# -ne 1 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 dir"
  exit $ERR_NARGS
fi

# 2) validar -h
if [ $1 -eq '-h' ]
then
  echo "Help de la ordre 07bis-directori"
  echo "@ isx41016667 asix m01 2021"
  exit 0
fi
  
# 3) validar si existeix directori
mydir=$1
if [ ! -d $mydir ]
then
  echo "Error: $mydir no es un directori"
  echo "Usage: $0 dir"
  exit $ERR_NODIR
fi

# xixa
ls $mydir
exit 0
