#! /bin/bash
# isx41016667 M01-ISO
# Febrer 2021
# Dir dies del mes dels mesos
# 	prog.sh mes[...]
# Validar que rep 1 o mes arguments
# Per cada mes [1-12]
# Per cada mes diu el num de dies 
# ----------------------------------------------------------------
ERR_NARG=1
OK=0

# 1) Validar arguments
if [ $# -eq 0 ]; then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 notes[...]"
  exit $ERR_NARG
fi

# 2) iterar per cada mes
for mes in $*
do
  # 2.1 validar mes
  case $mes in
    "1"|"3"|"5"|"7"|"8"|"10"|"12")
      echo "El mes $mes te 31 dies";;
    "4"|"6"|"9"|"11")
      echo "El mes $mes te 30 dies";;
    "2")
      echo "El mes $mes te 28 dies";;
    *)
      echo "Error: mes $mes no valida[1-12]" >> /dev/stderr
  esac
done
exit $OK
