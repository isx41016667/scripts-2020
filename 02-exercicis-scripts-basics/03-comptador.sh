#! /bin/bash
# isx41016667 M01-ISO
# Febrer 2021
# Fer un comptador des de zero fins al valor
# indicat per l'argument rebut.
#       prog.sh num
# Validar num argument
# Numera linia a linia fins al valor num
# -------------------------------------------
ERR_NARGS=1
# 1 validar num arguments
if [ $# -ne 1 ]
then
  echo "Error: num argument incorrecte"
  echo "Usage: $0 num"
  exit $ERR_NARGS
fi

MAX=$1
num=0
while [ $num -le $MAX ]
do
  echo $num
  ((num++))
done
exit 0
