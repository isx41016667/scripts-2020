#! /bin/bash
# isx41016667 M01-ISO
# Febrer 2021
# Mostrar linia a linia stdin, retallant nomes
# els primers 50 caracters.
#       prog.sh
# Validar linia a linia pepr stdin
# Mostrar nomes els primers 50 caracters
# -------------------------------------------
# 1 validar linia per stdin
while read -r linia
do
  # xixa
  echo $linia | cut -c1-50
done
exit 0
