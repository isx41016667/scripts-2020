#! /bin/bash
# isx41016667 M01-ISO
# Febrer 2021
# Compta els arguments que son dies laborals
# o festius
#       prog.sh arg[...]
# Validar num argument
# Mostrar num dies laborals i festius 
# -------------------------------------------
ERR_NARGS=1
# 1 validar num arguments
if [ $# -lt 1 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 args[...]"
fi

laborals=0
festius=0
for dia in $*
do
  case $dia in
    "dilluns"|"dimarts"|"Dimecres"|"Dijous"|"Divendres")
      ((laborals++));;
    "dissabte"|"diumenge")
      ((festius++);;
    *)
      echo Error: dia $dia incorrecte"" >> /dev/stderr
  esac
done

# xixa
echo "laborals: $laborals"
echo "festius: $festius"
exit 0
