#! /bin/bash
# isx41016667 M01-ISO
# Febrer 2021
# Mostra el stdin numerat linia a linia
#       prog.sh
# Validar linia stdin
# Numera linia a linia
# -------------------------------------------
num=0
# 1 valida linia per stdin
while read -r linia
do
  # xixa
  echo "$num- $linia"
  ((num++))
done
exit 0
