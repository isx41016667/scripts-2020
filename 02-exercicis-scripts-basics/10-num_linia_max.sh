#! /bin/bash
# isx41016667 M01-ISO
# Febrer 2021
# Rep per arguments un num indicatiu de maxim
# de linies a mostrar.
#       prog.sh num
# Validar num argument
# Mostrar linies permetides
# -------------------------------------------
ERR_NARGS=1
# 1 validar num arguments
if [ $# -ne 1 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 num"
  exit $ERR_NARGS
fi
MAX=$1

num=1
while read -r linia
do
  # xixa
  if [ $num -le $MAX ]
  then
    echo "$num: $linia"
  else
    echo $linia
  fi
  ((num++))
done
exit 0
