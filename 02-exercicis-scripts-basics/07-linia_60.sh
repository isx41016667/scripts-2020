#! /bin/bash
# isx41016667 M01-ISO
# Febrer 2021
# Mostrar les linies, de stdin, que tenen
# mes de 60 caracters.
#       prog.sh
# Validar linies per stdin
# Validar caracters
# Mostrar linia
# -------------------------------------------
# 1 validar linies per stdin
while read -r linia
do
  num=$(echo $linia | wc -c)
  # 2 validar caracters
  if [ $num -gt 60 ]
  then
    # xixa
    echo $linia
  fi
done
exit 0
