#! /bin/bash
# Exemples de funcions
# isx41016667 ISO 2020-21
# ==================================
# 1. showUser login
function showUser(){
  ERR_NARGS=1
  ERR_NLOGIN=2
  # 1) validar num arguments
  if [ $# -ne 1 ]
  then
    echo "Error: num arguments incorrecte"
    echo "Usage: $0 login"
    return $ERR_NARGS
  fi
  
  login=$1
  # 2) validar login
  linea=$( grep "^$login:" /etc/passwd 2> /dev/null)
  if [ -z $linea ]
  then
    echo "Error: login $login inexistent"
    echo "Usage: showUser login"
    return $ERR_NLOGIN
  fi

  uid=$(echo $linea | cut -d: -f3)
  gid=$(echo $linea | cut -d: -f4)
  gecos=$(echo $linea | cut -d: -f5)
  home=$(echo $linea | cut -d: -f6)
  shell=$(echo $linea | cut -d: -f7)
  # xixa
  echo "login: $login"
  echo "uid: $uid"
  echo "gid: $gid"
  echo "gecos: $gecos"
  echo "home: $home"
  echo "shell: $shell"
  return 0
}

# 3. showGroup gname
function showGroup(){
  ERR_NARGS=1
  ERR_NGNAME=2
  # validar num arguments
  if [ $# -ne 1 ]
  then
    echo "Error: num arguments incorrecte"
    echo "Usage: showGroup gname"
    return $ERR_NARGS
  fi

  gname=$1
  # validar gname
  linea=$(grep $gname /etc/group 2> /dev/null)
  if [ -z $linea ]
  then
    echo "Error: gname $gname inexistent"
    echo "Usage: showGroup gname"
    return $ERR_NGNAME
  fi

  gid=$(echo $linea | cut -d: -f3)
  users=$(echo $linea | cut -d: -f4)
  # xixa
  echo "gname: $gname"
  echo "gid: $gid"
  echo "users: $users"
  return 0
}

# 4. showUser2 login
function showUser2(){
  ERR_NARGS=1
  ERR_NLOGIN=2
  # 1) validar num arguments
  if [ $# -ne 1 ]
  then
    echo "Error: num arguments incorrecte"
    echo "Usage: $0 login"
    return $ERR_NARGS
  fi

  login=$1
  # 2) validar login
  linea=$( grep "^$login:" /etc/passwd 2> /dev/null)
  if [ -z $linea ]
  then
    echo "Error: login $login inexistent"
    echo "Usage: showUser login"
    return $ERR_NLOGIN
  fi

  uid=$(echo $linea | cut -d: -f3)
  gid=$(echo $linea | cut -d: -f4)
  gecos=$(echo $linea | cut -d: -f5)
  home=$(echo $linea | cut -d: -f6)
  shell=$(echo $linea | cut -d: -f7)
  # 3) trobar el gname
  gname=$(grep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1 2> /dev/null)
  
  # xixa
  echo "login: $login"
  echo "gname: $gname"
  echo "uid: $uid"
  echo "gid: $gid"
  echo "gecos: $gecos"
  echo "home: $home"
  echo "shell: $shell"
  return 0
}

# 5. showUserList login[...]
function showUserList(){
  status=0
  ERR_NARGS=1
  ERR_NLOGIN=2
  # 1) validar num arguments
  if [ $# -lt 1 ]
  then
    echo "Error: num arguments incorrecte"
    echo "Usage: showUserList login[...]"
    return $ERR_NARGS
  fi
 
LOGINS=$*
  for login in $LOGINS
  do
    # 2) validar login
    linea=$(grep "^$login:" /etc/passwd 2> /dev/null)
    if [ -z $linea ]
    then
      echo "Error: login $login inexistent" >> /dev/stderr
      status=$ERR_NLOGIN
    else
      uid=$(echo $linea | cut -d: -f3)
      gid=$(echo $linea | cut -d: -f4)
      gecos=$(echo $linea | cut -d: -f5)
      home=$(echo $linea | cut -d: -f6)
      shell=$(echo $linea | cut -d: -f7)
      # xixa
      echo "login: $login"
      echo "uid: $uid"
      echo "gid: $gid"
      echo "gecos: $gecos"
      echo "home: $home"
      echo "shell: $shell"
    fi
  done
  return $status
}

# 6. showUserIn < fileIn
function showUserIn(){
  status=0
  ERR_NLOGIN=1
  # 1) validar linea
  while read -r login
  do
    # 3) validar login
    linea=$(grep "^$login:" /etc/passwd 2> /dev/null)
    if [ -z $linea ]
    then
      echo "Error: login $login inexistent" >> /dev/stderr
      status=$ERR_NLOGIN
    else
      uid=$(echo $linea | cut -d: -f3)
      gid=$(echo $linea | cut -d: -f4)
      gecos=$(echo $linea | cut -d: -f5)
      home=$(echo $linea | cut -d: -f6)
      shell=$(echo $linea | cut -d: -f7)
      # xixa
      echo "login: $login"
      echo "uid: $uid"
      echo "gid: $gid"
      echo "gecos: $gecos"
      echo "home: $home"
      echo "shell: $shell"
    fi
  done
  return $status
}

# 7. showGroupMainMembers gname
function showGroupMainMembers(){
  status=0
  ERR_NARGS=1
  ERR_NGNAME=2
  # 1) valida num arguments
  if [ $# -ne 1 ]
  then
    echo "Error: num arguments incorrecte"
    echo "Usage: showGroupMembers gname"
    return $ERR_NARGS
  fi
  
  gname=$1
  # 2) validar gname
  linea=$(grep "^$gname:" /etc/group 2> /dev/null)
  if [ -z $linea ] 
  then
    echo "Error: gname $gname inexistent"
    return $ERR_NGNAME
  else
    gid=$(echo $linea | cut -d: -f3)
    # 3) trobar logins del gname
    LOGINS=$(grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1)
    for login in $LOGINS
    do
      linia=$(grep "^$login:" /etc/passwd | cut -d: -f1,3,6,7)
      #xixa
      echo $linia
    done
  fi
  return $status
}

# 8. showGroupMainMembers2 gname
function showGroupMainMembers2(){
  status=0
  ERR_NARGS=1
  ERR_NGNAME=2
  # 1) valida num arguments
  if [ $# -ne 1 ]
  then
    echo "Error: num arguments incorrecte"
    echo "Usage: showGroupMembers gname"
    return $ERR_NARGS
  fi

  gname=$1
  usuaris=''
  # 2) validar gname
  linea=$(grep "^$gname:" /etc/group 2> /dev/null)
  if [ -z $linea ]
  then
    echo "Error: gname $gname inexistent"
    return $ERR_NGNAME
  else
    gid=$(echo $linea | cut -d: -f3)
    # 3) trobar logins del gname
    LOGINS=$(grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1)
    for login in $LOGINS
    do
      linia=$(grep "^$login:" /etc/passwd | cut -d: -f1,3,6,7)
      # xixa
      echo "a) separades per un tab (cada camp d'un usuari)"
      echo "$linia" | tr ':' '\t'
      echo "b) separades per dos espais cada camp"
      echo "$linia" | sed 's/:/  /'
      echo "c) ordenat per login i separats per dos espais"
      echo "$linia" | sort | sed 's/:/  /'
      echo "d) ordenat per uid, separat per un espai i tot en majuscules"
      echo "$linia" | sort -t: -k2n | sed 's/:/ /' | tr '[a-z]' '[A-Z]' 
    done
  fi

  return $status
}

# 21. getHome login
function getHome(){
  # 1) trobar el home de login
  login=$1
  home=$( grep "^$login:" /etc/passwd | cut -d: -f6 )
  if [ -z $home ]
  then
    return 1
  fi
  echo $home
  return 0
}

# 22. getHoleList login[..]
function getHoleList(){
  ERR_NARGS=1
  ERR_NLOGIN=2
  status=0
  # 1) valdar num arguments
  if [ $# -lt 1 ]
  then
    echo "Error: num arguments incorrecte"
    echo "Usage: getHoleList login[..]"
    return $ERR_NARGS
  fi

  LOGINS=$*
  for login in $LOGINS
  do
    grep "^$login:" /etc/passwd &> /dev/null
    # 2) validar logins
    if [ $? -ne 0 ]
    then
      echo "Error: login $login no existeix" >> /dev/stderr
      status=$ERR_NLOGIN
    fi

    # xixa
    getHome $login
  done
  return $status
}

# 23. getSize homedir
function getSize(){
  ERR_NHOMEDIR=1
  status=0
  
  homedir=$1
  # 1) validar homedir
  if ! [ -d $homedir ]
  then
    echo "Error: homedir $homedir no existe"
    echo "Usage: getSize homedir"
    return $ERR_NHOMEDIR
  fi
  
  # xixa
  du -b $homedir 2> /dev/null | cut -f1
  return $status
}

# 24. getSizeIn
function getSizeIn(){
  ERR_NLOGIN=1
  status=0

  # 1) legir login per stdout
  while read -r login
  do
    # 2) validar login
    grep "$login:" /etc/passwd &> /dev/null
    if [ $? -ne 0 ]
    then
      echo "Error: login $login no existeix"
      status=$ERR_NLOGIN
    else
      home=$( getHome $login )
      getSize $home
    fi
  done
  return $status
}

# 25. getAllUsersSize
function getAllUsersSize(){
  LOGINS=$(cat /etc/passwd | cut -d: -f1)
  for login in $LOGINS
  do
    echo "$login:"
    grep "^$login:" /etc/passwd | cut -d: -f6
  done
}
# ------------------------------------------------------------------------------
function showAllGroups(){
  MIN_USERS=2
  llista_gids=$(cut -d: -f4 /etc/passwd | sort -n | uniq )
  for gid in $llista_gids
  do
    gname=$(grep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)
    count=$(grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | wc -l)
    if [ $count -ge $MIN_USERS ] 
    then
      echo "$gname($gid): $count"
      grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | sort | cut -d: -f 1,3,7 | sed 's/^/\t/'
    fi 
  done
}

function creaClasse(){
  classe=$1
  PASSWD="alum"
  llista_noms=$(echo ${classe}{01..02})
  for user in $llista_noms
  do
    useradd $user
    echo "$user:$PASSWD" | chpasswd
    #echo "alum" | passwd --stdin $user &> /dev/null
  done
}

function creaEscola(){
  for classe in $*
  do
    creaClasse $classe
  done
}
# ------------------------------------------------------------------------------
# funcions d'exemple
function suma(){
  echo "La suma es: $(($1+$2))"
  exit 0
}

function dia(){
  date
  return 0
}
