#! /bin/bash
# isx41016667 M01-ISO
# Març 2021
# Processar els arguments i comptar quantes
# n'hi ha de 3 o més caràcters
#       prog.sh args[...]
# Validar num arguments
# Comptar arguments de mes de 3 caracters
# -------------------------------------------
ERR_NARGS=1
# 1 validar num arguments
if [ $# -lt 1 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 args[...]"
  exit $ERR_NARGS
fi

num=0
for arg in $*
do
  # xixa
  echo $arg | egrep '.{3,}' &> /dev/null
  if [ $? -eq 0 ]
  then
    ((num++))
  fi
done
echo "hi ha $num args de mes de 3 caracters"
exit 0
