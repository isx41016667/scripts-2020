#! /bin/bash
# isx41016667 M01-ISO
# Març 2021
# Processar arguments que són matricules:
#       prog.sh args[...]
# Validar num arguments
# Mostrar matricula
# -------------------------------------------
ERR_NARGS=1
ERR_NMAT=2
status=0
# 1 validar num arguments
if [ $# -lt 1 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 args[...]"
  exit $ERR_NARGS
fi

for matricula in $*
do
  echo $matricula | egrep "^[0-9]{4} [A-Z]{3}$" 2> /dev/null
  if [ $? -ne 0 ]
  then
    echo "Error: matricula $matricula no valida" >> /dev/stderr
    status=$ERR_NMAT
  fi
done
exit $status
