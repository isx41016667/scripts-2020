#! /bin/bash
# @edt ASIX-M01 Curs 2018-2019
# exercici 11
###################################################################
status=0
if [ $# -eq 1 -a "$1" = "-h" ]; then
  echo "mostrant el help"
  exit 0
fi
if [ $# -eq 0 ]; then
  echo "error num args..."
  echo "usage..."
  exit 1
fi
for uid in $*
do 
  ulinia=$(grep "^[^:]*:[^:]*:$uid:" /etc/passwd)
  if [ $? -eq 0 ]; then
    login=$(echo $ulinia | cut -d: -f1) 
    gid=$(echo $ulinia | cut -d: -f4) 
    gname=$(grep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)
    dirHome=$(echo $ulinia | cut -d: -f6) 
    shell=$(echo $ulinia | cut -d: -f7) 
    echo "$login($uid) $gname $dirHome $shell"
  else
    ecgo "error $uid inexistent" >> /dev/stderr
  fi
done
exit $status





